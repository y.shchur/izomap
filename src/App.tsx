import { Feature, Polygon } from "@turf/helpers";
import { union } from "@turf/turf";
import Axios, { AxiosInstance } from "axios";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import React from "react";
import "./App.css";
const _ = require("lodash");

export class App extends React.Component<any, any> {
  map: L.Map;
  accessLayer: any;
  axios: AxiosInstance;

  constructor(props: any) {
    super(props);
    this.axios = Axios.create({
      headers: {
        Authorization:
          "5b3ce3597851110001cf624818506489006640b2a02b536655d3a223",
      },
    });
  }

  componentDidMount() {
    this.map = L.map("izomap").setView([51.108935, 17.032271], 13);
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.map);
    const icon = L.icon({ iconUrl: "location_on-24px.svg" });
    const markers: number[][] = [];
    this.map.on("click", (e: any) => {
      const latLng = L.marker(e.latlng).setIcon(icon).addTo(this.map).getLatLng();
      markers.push([latLng.lng, latLng.lat]);
      this.createIzochrones(markers);
    });
  }

  private createIzochrones(locations:  number[][]) {
 
    this.axios
      .post("https://api.openrouteservice.org/v2/isochrones/foot-walking", {
        locations,
        range: [660, 460, 330, 200],
        attributes: ["reachfactor"],
        range_type: "distance",
      })
      .then((res: any) => {
        if(this.map.hasLayer(this.accessLayer)){
          this.map.removeLayer(this.accessLayer);
        }
        const groupedByDist = (_.groupBy(
          res.data.features,
          (f: any) => f.properties.value
        ) as any) as { number: Feature<Polygon>[] };
        console.log(groupedByDist);
        const sum = Object.values(
          groupedByDist
        ).map((features: Feature<Polygon>[]) => union(...features));
        const data = { ...res.data, features: sum };
        this.accessLayer = L.geoJSON(data).addTo(this.map);
      })
      .catch((err) => console.log(err));
  }

  render() {
    return (
      <div>
        <div id="izomap"></div>;
      </div>
    );
  }
}

export default App;
